CC=		gcc
CFLAGS= 	-g -std=gnu99 -D_GNU_SOURCE -Wall -I.
LD=		gcc
LDFLAGS= 	-L.

C_SOURCE= 	$(wildcard *.c)
OBJECTS= 	$(C_SOURCE:.c=.o)
PROGRAMS= 	ircle

all:	$(OBJECTS) $(PROGRAMS)

%.o:	%.c
	$(CC) $(CFLAGS) -o $@ -c $^

ircle:	ircle.o socket.o
	$(LD) $(LDFLAGS) -o $@ $^

clean:
	rm -f $(PROGRAMS) $(OBJECTS)

.PHONY:	all clean
