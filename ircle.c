/* ircle.c: IRC bot */

#include "socket.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Constants */

static const char *NICK    = "ircle2099";
static const char *HOST    = "irc.freenode.net";
static const char *PORT    = "6667";
static const char *CHANNEL = "#uwec-cs";

static const char *MESSAGES[] = {
    "Ready to serve.",
    "Yes?",
    "My lord?",
    "What is it?",
    "Hello.",
    "Okay.",
    "Right.",
    "Alright.",
    "Yes my lord!",
    "Job's done.",
    "Oh, what?",
    "Y—uh-huh.",
    "Now what?",
    "More work?",
    "Leave me alone!",
    "I don't want to do this!",
    "I'm not listening.",
    NULL
};

/* Functions */

void read_popen(const char *command, FILE *stream) {
    FILE *process = popen(command, "r");
    if (!process) {
    	perror("popen");
    	return;
    }

    char  buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, process)) {
	buffer[strlen(buffer) - 1] = 0;
	fprintf(stream, "PRIVMSG %s :%s\r\n", CHANNEL, buffer);
    }
    pclose(process);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int socket_fd = socket_dial(HOST, PORT);
    if (socket_fd < 0) {
    	perror("socket_dial");
    	return EXIT_FAILURE;
    }

    FILE *socket_file = fdopen(socket_fd, "r+");
    if (!socket_file) {
    	perror("fdopen");
    	return EXIT_FAILURE;
    }

    char  buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, socket_file)) {
    	fputs(buffer, stdout);

    	if (strstr(buffer, ":*** Found your hostname")) {
    	    fprintf(socket_file, "USER %s 0 * :IRCLE 2099\r\n", NICK);
    	    fprintf(socket_file, "NICK %s\r\n", NICK);
	} else if (strstr(buffer, ":+i") && strstr(buffer, NICK)) {
	    fprintf(socket_file, "JOIN %s\r\n", CHANNEL);
	} else if (strncmp(buffer, "PING :", 6) == 0) {
	    fprintf(socket_file, "PONG %s\r\n", strstr(buffer, ":"));
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, NICK)) {
	    fprintf(socket_file, "PRIVMSG %s :%s\r\n",
	    	    CHANNEL,
	    	    MESSAGES[rand() % sizeof(MESSAGES) / sizeof(char *)]);
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, "!who")) {
	    read_popen("w", socket_file);
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, "!uptime")) {
	    read_popen("uptime", socket_file);
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, "!uname")) {
	    read_popen("uname -a", socket_file);
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, "!write")) {
	    char *payload = strstr(buffer, "!write");
	    char *user    = strtok(payload + 7, " ");
	    char *message = user + strlen(user) + 1;
	    char  command[BUFSIZ];
	    FILE *process;

	    snprintf(command, BUFSIZ, "write %s", user);
	    process = popen(command, "w");
	    if (process) {
	    	fputs(message, process);
		pclose(process);
	    }
	} else if (strstr(buffer, "PRIVMSG #uwec-cs") && strstr(buffer, "!falldown")) {
	    fclose(socket_file);
	    execvp(argv[0], argv);
	    _exit(EXIT_FAILURE);
	}
    }

    return EXIT_SUCCESS;
}
