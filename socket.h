/* socket.h: simple socket library */

#ifndef SOCKET_H
#define SOCKET_H

#include <netdb.h>

int socket_resolve(const char *host, const char *port, struct addrinfo **hostinfo);
int socket_dial(const char *host, const char *port);

#endif
