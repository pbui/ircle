/* socket.c: simple socket library */

#include <string.h>

#include <netdb.h>
#include <unistd.h>

int socket_resolve(const char *host, const char *port, struct addrinfo **hostinfo) {
    struct addrinfo hints = {
	.ai_family   = AF_UNSPEC,	/* Use either IPv4 (AF_INET) or IPv6 (AF_INET6) */
	.ai_socktype = SOCK_STREAM,	/* Use TCP  */
    };

    return getaddrinfo(host, port, &hints, hostinfo);
}

int socket_dial(const char *host, const char *port) {
    struct addrinfo *hostinfo;
    int socket_fd = -1;

    if (socket_resolve(host, port, &hostinfo) < 0) {
    	return -1;
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = hostinfo; p && socket_fd < 0; p = p->ai_next) {
	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    continue;
	}

	/* Connect to host */
	if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    close(socket_fd);
	    socket_fd = -1;
	}
    }

    freeaddrinfo(hostinfo);
    return socket_fd;
}
